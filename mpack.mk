DEPS:=deps
MPACK:=$(DEPS)/mpack
SRC_MPACK:=$(MPACK)/src/mpack
BUILD_MPACK:=$(patsubst $(DEPS)/%,$(TARGETS)/%,$(MPACK))

SOURCES_MPACK+=$(SRC_MPACK)/mpack-common.c
SOURCES_MPACK+=$(SRC_MPACK)/mpack-expect.c
SOURCES_MPACK+=$(SRC_MPACK)/mpack-node.c
SOURCES_MPACK+=$(SRC_MPACK)/mpack-platform.c
SOURCES_MPACK+=$(SRC_MPACK)/mpack-reader.c
SOURCES_MPACK+=$(SRC_MPACK)/mpack-writer.c

OBJECTS_MPACK:=$(patsubst $(SRC_MPACK)/%,$(BUILD_MPACK)/%.o,$(SOURCES_MPACK))
OBJDEPS_MPACK:=$(patsubst %.o,%.d,$(OBJECTS_MPACK))

-include $(OBJDEPS_MPACK)

INCLUDE+=-I$(SRC_MPACK)

$(OBJECTS_MPACK): | $(BUILD_MPACK)/
$(OBJDEPS_MPACK): | $(BUILD_MPACK)/

$(TARGETS)/libmpack.a: $(OBJECTS_MPACK)
	@ echo "  AR      $@"
	$(Q) $(AR) rcs $@ $^

$(BUILD_MPACK)/%.c.o: $(SRC_MPACK)/%.c
	@ echo "  CC      $@"
	$(Q) $(CC) $(CFLAGS) -c -o $@ $<

.PHONY: claen-mpack
clean-mpack:
	$(Q) $(RM) -rfv $(TARGETS)/libmpack.a \
	$(OBJECTS_MPACK) \
	$(OBJDEPS_MPACK)
