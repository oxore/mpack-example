Q=@
RM=rm

TARGET_MAIN=main
TARGET_TEST=test
MUNIT_OBJECT=deps/munit/munit.c.o

all:

SRC=src
BUILD=build
TARGETS=$(BUILD)/targets

SOURCES+=$(SRC)/a.c
OBJECTS:=$(patsubst $(SRC)/%,$(BUILD)/%.o,$(SOURCES))

INCLUDE+=./include
INCLUDE+=./deps/munit
INCLUDE:=$(patsubst %,-I%,$(INCLUDE))

#COMMONFLAGS+=-fsanitize=address
#COMMONFLAGS+=--coverage

CFLAGS+=$(INCLUDE)
CFLAGS+=$(COMMONFLAGS)
CFLAGS+=-Wall
CFLAGS+=-Wextra
CFLAGS+=-Wpedantic
CFLAGS+=-Wshadow
CFLAGS+=-Wduplicated-branches
CFLAGS+=-Wduplicated-cond
CFLAGS+=-O3
CFLAGS+=-g3
CFLAGS+=-std=c11

LDFLAGS+=$(COMMONFLAGS)
LDFLAGS+=-Wl,--gc-section

LDFLAGS_MAIN+=$(LDFLAGS)

%/:
	$(Q) mkdir -p $@

-include mpack.mk

all: $(TARGET_MAIN)
ifndef NOTEST
all: $(TARGET_TEST)
endif

$(TARGET_MAIN): $(OBJECTS) $(TARGETS)/$(TARGET_MAIN).c.o $(TARGETS)/libmpack.a
	@ echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_SINGLE) -o $@ $^

$(TARGET_TEST): $(OBJECTS) $(TARGETS)/$(TARGET_TEST).c.o $(MUNIT_OBJECT)
	@ echo "  LD      $@"
	$(Q) $(CC) $(LDFLAGS_MAIN) -o $@ $^

$(MUNIT_OBJECT): $(patsubst %.c.o,%.c,$(MUNIT_OBJECT))
	@ echo "  CC      $@"
	$(Q) $(CC) -c -o $@ $<

$(OBJECTS): | $(BUILD)/
$(OBJECTS): | $(TARGETS)/

$(BUILD)/%.c.o: $(SRC)/%.c
	@ echo "  CC      $@"
	$(Q) $(CC) $(CFLAGS) -c -o $@ $<

clean: clean-mpack
	$(Q) $(RM) -rfv \
		$(TARGET_MAIN) \
		$(TARGET_TEST) \
		$(TARGETS)/$(TARGET_MAIN).c.o \
		$(TARGETS)/$(TARGET_TEST).c.o \
		$(OBJECTS) \
		$(MUNIT_OBJECT)

mrproper: clean
	$(Q) $(RM) -rf $(BUILD)

.PHONY: clean mrproper all
